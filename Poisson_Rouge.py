# Importation des modules Pygame et OS
import pygame, os

class Dessin:
    """
    classe pour un objet dessinable, éventuellement sonorisé
    """
    
    def __init__(self, fichier, son, volume, x, y, mouvement=0):
        """
        constructeur
        :param fichier: chemin complet vers un fichier graphique
        :param son: chemin complet vers un fichier son
        :param volume: la force du son
        :param x: abscisse initiale
        :param y: ordonnée initiale
        :param mouvement: vitesse horizontale (0 par défaut)
        """
        self.image=pygame.image.load(fichier)
        self.mouvement = mouvement
        self.x=x
        self.y =y
        if son:
            self.son=pygame.mixer.music.load(son)
            pygame.mixer.music.set_volume(volume)
            pygame.mixer.music.play(loops=-1)
    
    def dessine(self):
        """
        dessine le dessin en x,y
        """
        fenetre.blit(self.image,(round(self.x),round(self.y)))
        
    def echelle(self, w, h):
        """
        Mise à l'échelle du dessin
        :param w: largeur
        :param h: hauteur
        """
        self.image = pygame.transform.scale(self.image, (h,w))
        
    def retourneGD(self):
        """
        retourne l'image de gauche à droite ou inversement
        """
        self.image = pygame.transform.flip(self.image, True, False)
        
    def regle_vitesse(self, valeur):
        """
        règle la vitesse horizontale
        :param valeur: valeur de la vitesse
        """
        self.mouvement = valeur
        
    def avance(self):
        """
        Gère le déplacement entre deux affichages
        """
        self.x += self.mouvement
        
        

        



if __name__=="__main__":
    # Initialisation de la bibliothèque Pygame
    # Parammétrage du mixer, réduction de la taille de la mémoire tampon
    # Consommation des ressources processeur plus grande, mais latence réduite lorsque le son est joué
    pygame.mixer.pre_init(44100, -16, 2, 1024)
    pygame.mixer.init()
    pygame.init()

    # Définition de la résolution de la fenêtre et du nombre d'images par seconde
    largeur = 1280
    hauteur = 720
    ips = 60

    # Attribution d'une couleur à l'arrière-plan
    # eau = (77, 84, 181)

    # Affichage de la fenêtre et de son titre
    fenetre = pygame.display.set_mode((largeur,hauteur))
    pygame.display.set_caption("Poisson Rouge")

    # Chargement de l'image d'arrière-plan
    img_background = Dessin(
        "/media/user/INTENSO/Le_poisson/Background.png",
        "/media/user/INTENSO/Le_poisson/Super_Mario_Bros_Music_-_Underwater.mp3",
        0.1,
        0,
        0
    )

    img_poisson2 = Dessin(
        "/media/user/INTENSO/Le_poisson/poisson_rouge.png",
        None,
        0,
        largeur//2+100,
        hauteur//2+200
    )
    img_poisson2.echelle(160, 320)
    img_poisson2.retourneGD()
    
    img_poisson = Dessin(
        "/media/user/INTENSO/Le_poisson/poisson_rouge.png",
        None,
        0,
        largeur//2,
        hauteur//2
    )
    img_poisson.echelle(160, 320)
    img_poisson.retourneGD()
    

    # Temps que met le poisson pour parcourir la fenêtre, à partir duquel on peut détermiiner sa vitesse en pixel par seconde
    temps_trajet = 10
    pixels_par_seconde = (largeur - 320) / temps_trajet

    """""
    Initialisation d'un objet qui va nous aider à récupérer des informations sur le temps passé, afin de baser notre jeu
    non pas sur le nombre d'images par seconde mais sur le temps écoulé
    Par exemple, la vitesse du poisson est identique sur n'importe quelle machine
    Une perte d'ips ne ralentira pas le déplacement du poisson
    Pour s'en asssurer, on peut modifier la valeur de la variable ips et chronométrer si le temps de trajet est toujours identique
    """
    clock = pygame.time.Clock()
    # Initialisation des variables déterminant la position primaire du poisson ainsi que sa vitesse de déplacement
    x = largeur/2 - 80
    y = hauteur/2 - 160

    # Booléen qui permettra d'exécuter du code une unique fois dans la boucle
    debut = True

    # Chargement de la musique et du son, réglage du volume et du mode répétition
    SoundFX = pygame.mixer.Sound("/media/user/INTENSO/Le_poisson/Super_Mario_Bros_Music_-_Swimming.wav")

    # Booléen permettant de sortir de la boucle et de quitter le jeu si sa valeur passe à True
    fin_jeu = False
    # Tant que ce dernier n'est pas vrai, on exécute ce qui est dans la boucle
    while not fin_jeu:

        # Met à jour l'horloge et permet de connaître la durée, en seconde, qui s'est écoulée entre 2 images (delta time)
        dt = clock.tick(ips) / 1000

        # On regarde dans la file des événements si le joueur quitte le jeu, et si c'est le cas, on sort de la boucle
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                fin_jeu = True

        # Initie la vitesse de départ du poisson
        if debut == True:
            img_poisson.regle_vitesse(pixels_par_seconde * dt)
            debut = False

        # Si le poisson atteint la bordure gauche
        if x <= 0:
            # On joue le son, on inverse sa vitesse de déplacement et son image
            SoundFX.play()
            img_poisson.regle_vitesse(pixels_par_seconde * dt)
            img_poisson.retourneGD()

        # Si le poisson atteint la bordure droite
        if x >= (largeur - 320):
            # On jour le son, on inverse sa vitesse de déplacement et son image
            SoundFX.play()
            img_poisson.retourneGD()
            img_poisson.regle_vitesse(-(pixels_par_seconde) * dt)


        # Modification de la position du poisson
        x += img_poisson.mouvement

        # Affichage de l'arrière-plan et du poisson
        # fenetre.fill(eau)
        
        img_background.dessine()
        img_poisson.dessine()
        img_poisson.avance()
        img_poisson2.dessine()
        img_poisson2.avance()

        # Rafraichissement de l'affichage
        pygame.display.update()

    # Sortie de la boucle, on quitte le jeu
    pygame.quit()
    quit()
