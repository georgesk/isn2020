# Importation des modules Pygame et OS
import pygame, os

# Initialisation de la bibliothèque Pygame
# Parammétrage du mixer, réduction de la taille de la mémoire tampon
# Consommation des ressources processeur plus grande, mais latence réduite lorsque le son est joué
pygame.mixer.pre_init(44100, -16, 2, 1024)
pygame.mixer.init()
pygame.init()

# Définition de la résolution de la fenêtre et du nombre d'images par seconde
largeur = 1280
hauteur = 720
ips = 60

# Attribution d'une couleur à l'arrière-plan
# eau = (77, 84, 181)

# Affichage de la fenêtre et de son titre
fenetre = pygame.display.set_mode((largeur,hauteur))
pygame.display.set_caption("Poisson Rouge")

# Chargement de l'image d'arrière-plan
img_background = pygame.image.load(os.path.join("Le_poisson", "/media/user/INTENSO/Le_poisson/Background.png"))

# Initialisation des dimensions du poisson
img_poisson_hauteur = 160
img_poisson_longueur = 320

# Chargement de l'image du poisson et modification de ses dimensions
img_poisson = pygame.image.load(os.path.join("Le_poisson", "/media/user/INTENSO/Le_poisson/poisson_rouge.png"))
img_poisson = pygame.transform.scale(img_poisson, (img_poisson_longueur, img_poisson_hauteur))
img_poisson = pygame.transform.flip(img_poisson, True, False)

# Temps que met le poisson pour parcourir la fenêtre, à partir duquel on peut détermiiner sa vitesse en pixel par seconde
temps_trajet = 10
pixels_par_seconde = (largeur - img_poisson_longueur) / temps_trajet

"""""
Initialisation d'un objet qui va nous aider à récupérer des informations sur le temps passé, afin de baser notre jeu
non pas sur le nombre d'images par seconde mais sur le temps écoulé
Par exemple, la vitesse du poisson est identique sur n'importe quelle machine
Une perte d'ips ne ralentira pas le déplacement du poisson
Pour s'en asssurer, on peut modifier la valeur de la variable ips et chronométrer si le temps de trajet est toujours identique
"""
clock = pygame.time.Clock()

# Création de deux fonctions, pour le poisson et le fond
def poisson (x,y,image):
    """
    On dessine le poisson
    :parm x: abscisse du poisson
    :param y: ordonnée du poisson
    :param image: l'image à dessiner
    """
    fenetre.blit(image,(round(x),round(y)))

def fond (x,y,image):
    fenetre.blit(image,(round(x),round(y)))

# Initialisation des variables déterminant la position primaire du poisson ainsi que sa vitesse de déplacement
x = largeur/2 - img_poisson_longueur/2
y = hauteur/2 - img_poisson_hauteur/2
x_mouvement = 0

# Booléen qui permettra d'exécuter du code une unique fois dans la boucle
debut = True

# Chargement de la musique et du son, réglage du volume et du mode répétition
pygame.mixer.music.load("/media/user/INTENSO/Le_poisson/Super_Mario_Bros_Music_-_Underwater.mp3")
pygame.mixer.music.set_volume(0.1)
pygame.mixer.music.play(loops=-1)
SoundFX = pygame.mixer.Sound("/media/user/INTENSO/Le_poisson/Super_Mario_Bros_Music_-_Swimming.wav")

# Booléen permettant de sortir de la boucle et de quitter le jeu si sa valeur passe à True
fin_jeu = False


# Tant que ce dernier n'est pas vrai, on exécute ce qui est dans la boucle
while not fin_jeu:

    # Met à jour l'horloge et permet de connaître la durée, en seconde, qui s'est écoulée entre 2 images (delta time)
    dt = clock.tick(ips) / 1000

    # On regarde dans la file des événements si le joueur quitte le jeu, et si c'est le cas, on sort de la boucle
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            fin_jeu = True

    # Initie la vitesse de départ du poisson
    if debut == True:
        x_mouvement = pixels_par_seconde * dt
        debut = False

    # Si le poisson atteint la bordure gauche
    if x <= 0:
        # On joue le son, on inverse sa vitesse de déplacement et son image
        SoundFX.play()
        x_mouvement = pixels_par_seconde * dt
        img_poisson = pygame.transform.flip(img_poisson, True, False)

    # Si le poisson atteint la bordure droite
    if x >= (largeur - img_poisson_longueur):
        # On jour le son, on inverse sa vitesse de déplacement et son image
        SoundFX.play()
        img_poisson = pygame.transform.flip(img_poisson, True, False)
        x_mouvement = -(pixels_par_seconde) * dt


    # Modification de la position du poisson
    x += x_mouvement

    # Affichage de l'arrière-plan et du poisson
    # fenetre.fill(eau)
    fond(0,0,img_background)
    poisson(x,y,img_poisson)

    # Rafraichissement de l'affichage
    pygame.display.update()

# Sortie de la boucle, on quitte le jeu
pygame.quit()
quit()
